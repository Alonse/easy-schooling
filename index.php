<?php

include_once ('config.php');


if($_POST['exit'] == 'ok'){
    unset($_SESSION);
    header("location: /auth.php");
};
?>

<?php include_once ('header.php'); ?>

  <?php include_once ('sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <section class="content-header">

    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-9">
            <div class="card">
            <div class="card-header">
              <h3 class="card-title">Список учебников</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="" method="GET">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>Название</th>
                      <th>Класс</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <?php
                        $class=$_SESSION['class'];
                        $id = $_GET['id'];
                        $userid = intval($_SESSION['id']);

                        if($_SESSION['role'] == 'student'){
                          $result=$pdo->query("SELECT * FROM `books` WHERE `class_book`='$class'");
                          while ($row=$result->fetch())
                          {
                              echo '<tr><td>'.$row['name_book'].'</td><td>'.$row['class_book'].'</td><td><a name="go" class="btn btn-primary" href="book.php?id='.$row['id'].'">Открыть</a>';
                          }
                        }
                        if($_SESSION['role'] == 'teacher') {
                          $result=$pdo->query(
                            "SELECT id,name_book,class_book, cc  
                            FROM `books` 
                            left join (
                              select bookid as book, count(*) as cc
                              from selections
                              where selections.teacherid = $userid
                              group by book
                              ) comments 
                              on comments.book = books.id
                            WHERE `class_book`='$class'
                            ");

                          while ($row=$result->fetch()) {?>
                             <tr>
                               <td>
                                <?=$row['name_book']?><br/>
                                Comments: <?=$row['cc']?>
                               </td>
                               <td>
                                 <?=$row['class_book']?>
                               </td>
                               <td>
                                <a name="go" class="btn btn-primary" href="book.php?id=<?=$row['id']?>">Открыть</a>
                               </td>
                            </tr>
                          <?}
                        }

                        if($_SESSION['role'] == 'admin') {
                            $result=$pdo->query("SELECT * FROM `books`");
                            while ($row=$result->fetch()){
                            echo '<tr><td>'.$row['name_book'].'</td><td>'.$row['class_book'].'</td><td><a name="go" class="btn btn-primary" href="book.php?id='.$row['id'].'">Открыть</a>';
                            echo '<a  class="btn btn-danger" style="position: relative; left: 10px;" name="delete" id="delete" href="index.php?id=' . $row['id'] . '"><img src="https://img.icons8.com/office/16/000000/delete.png"></a></td></tr>';
                            if($_GET['id'] != ''){
                                $sql="DELETE FROM `books` WHERE `books`.`id`=$id";
                                $pdo->query($sql);}
                            }
                        }
                        ?>
                    </tr>
                    </tfoot>
                  </table>
                </form>
            </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
<?php include_once ('footer.php');?>
