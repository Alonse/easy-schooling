<?php
session_start();
include_once ('config.php');
if($_POST['exit'] == 'ok'){
    unset($_SESSION);
    header("location: /auth.php");
};

// Название <input type="file">
$input_name = 'file';

// Разрешенные расширения файлов.
$allow = array('jpg', 'gif', 'png', 'jpeg');

// Запрещенные расширения файлов.
$deny = array();

// Директория куда будут загружаться файлы.
$path = __DIR__ . '/upload/avatar/';

if (isset($_FILES[$input_name])) {
    // Проверим директорию для загрузки.
    if (!is_dir($path)) {
        mkdir($path, 0777, true);
    }

    // Преобразуем массив $_FILES в удобный вид для перебора в foreach.
    $files = array();
    $diff = count($_FILES[$input_name]) - count($_FILES[$input_name], COUNT_RECURSIVE);
    if ($diff == 0) {
        $files = array($_FILES[$input_name]);
    } else {
        foreach($_FILES[$input_name] as $k => $l) {
            foreach($l as $i => $v) {
                $files[$i][$k] = $v;
            }
        }
    }

    foreach ($files as $file) {
        $error = $success = '';

        // Проверим на ошибки загрузки.
        if (!empty($file['error']) || empty($file['tmp_name'])) {
            switch (@$file['error']) {
                case 1:
                case 2: $error = 'Превышен размер загружаемого файла.'; break;
                case 3: $error = 'Файл был получен только частично.'; break;
                case 4: $error = 'Файл не был загружен.'; break;
                case 6: $error = 'Файл не загружен - отсутствует временная директория.'; break;
                case 7: $error = 'Не удалось записать файл на диск.'; break;
                case 8: $error = 'PHP-расширение остановило загрузку файла.'; break;
                case 9: $error = 'Файл не был загружен - директория не существует.'; break;
                case 10: $error = 'Превышен максимально допустимый размер файла.'; break;
                case 11: $error = 'Данный тип файла запрещен.'; break;
                case 12: $error = 'Ошибка при копировании файла.'; break;
                default: $error = 'Файл не был загружен - неизвестная ошибка.'; break;
            }
        } elseif ($file['tmp_name'] == 'none' || !is_uploaded_file($file['tmp_name'])) {
            $error = 'Не удалось загрузить файл.';
        } else {
            // Оставляем в имени файла только буквы, цифры и некоторые символы.
            $pattern = "[^a-zа-яё0-9,~!@#%^-_\$\?\(\)\{\}\[\]\.]";
            $name = mb_eregi_replace($pattern, '-', $file['name']);
            $name = mb_ereg_replace('[-]+', '-', $name);

            // Т.к. есть проблема с кириллицей в названиях файлов (файлы становятся недоступны).
            // Сделаем их транслит:
            $converter = array(
                'а' => 'a',   'б' => 'b',   'в' => 'v',    'г' => 'g',   'д' => 'd',   'е' => 'e',
                'ё' => 'e',   'ж' => 'zh',  'з' => 'z',    'и' => 'i',   'й' => 'y',   'к' => 'k',
                'л' => 'l',   'м' => 'm',   'н' => 'n',    'о' => 'o',   'п' => 'p',   'р' => 'r',
                'с' => 's',   'т' => 't',   'у' => 'u',    'ф' => 'f',   'х' => 'h',   'ц' => 'c',
                'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',  'ь' => '',    'ы' => 'y',   'ъ' => '',
                'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

                'А' => 'A',   'Б' => 'B',   'В' => 'V',    'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
                'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',    'И' => 'I',   'Й' => 'Y',   'К' => 'K',
                'Л' => 'L',   'М' => 'M',   'Н' => 'N',    'О' => 'O',   'П' => 'P',   'Р' => 'R',
                'С' => 'S',   'Т' => 'T',   'У' => 'U',    'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
                'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',  'Ь' => '',    'Ы' => 'Y',   'Ъ' => '',
                'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
            );

            $name = strtr($name, $converter);
            $parts = pathinfo($name);

            if (empty($name) || empty($parts['extension'])) {
                $error = 'Недопустимое тип файла';
            } elseif (!empty($allow) && !in_array(strtolower($parts['extension']), $allow)) {
                $error = 'Недопустимый тип файла';
            } elseif (!empty($deny) && in_array(strtolower($parts['extension']), $deny)) {
                $error = 'Недопустимый тип файла';
            } else {
                // Чтобы не затереть файл с таким же названием, добавим префикс.
                $i = 0;
                $prefix = '';
                while (is_file($path . $parts['filename'] . $prefix . '.' . $parts['extension'])) {
                    $prefix = '(' . ++$i . ')';
                }
                $name = $parts['filename'] . $prefix . '.' . $parts['extension'];
                $success = 'Файл «' . $name . '» успешно загружен.';
                // Перемещаем файл в директорию.
                if (($_POST['sent']!='')&&(move_uploaded_file($file['tmp_name'], $path . $name))) {
                    // Далее можно сохранить название файла в БД и т.п.
                    $id=$_SESSION['id'];
                    $sql="UPDATE `users` SET `image`='$name' WHERE `id`='$id'";
                    $pdo->query($sql);
                    $_SESSION['image']=$name;
                } else {
                    $error = 'Не удалось загрузить файл.';
                }
            }
        }
    }
}
require ('header.php');

require  ('sidebar.php');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Профиль</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Главная</a></li>
              <li class="breadcrumb-item active">Профиль</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-warning card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                    <?php
                    $image=$_SESSION['image'];
                   echo '<img src="upload/avatar/'.$image.'" class="profile-user-img img-fluid img-circle">';
                    ?>
                   </div>

                  <?php
                      echo '<h3 class="profile-username text-center">'.$_SESSION['name'].' '.$_SESSION['surname'].'</h3>';
                  ?>
                <ul class="list-group list-group-unbordered mb-3">
                    <?php
                        echo '<li class="list-group-item"><b>Город</b><a class="float-right">'.$_SESSION['city'].'</a></li>';
                        echo '<li class="list-group-item"><b>Школа</b><a class="float-right">'.$_SESSION['school'].'</a></li>';
                        echo '<li class="list-group-item"><b>Класс</b><a class="float-right">'.$_SESSION['class'].'</a></li>';
                    ?>
                </ul>
                  <form method="POST" id="js-form" enctype="multipart/form-data">
                      <input id="js-file" type="file" name="file[]" multiple">
                      <button type="submit" class="btn btn-primary" name='sent' value='profile.php?id='$row['id']'>Добавить</button>
                  </form>
<!--                  <div id="result">-->
<!--                      --><?php
//                      if (!empty($success)) {
//                          echo '<p>' . $success . '</p>';
//                      } else {
//                          echo '<p>' . $error . '</p>';
//                      }
//                      ?>
<!--                  </div>-->
              </div>
              <!-- /.card-body -->
            </div>
              <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
              <script src="//malsup.github.io/min/jquery.form.min.js"></script>

              <script>
                  $('#js-file').change(function() {
                      $('#js-form').ajaxSubmit({
                          type: 'POST',
                          url: '/profile.php',
                          target: '#result',
                          success: function() {
                              // После загрузки файла очистим форму.
                              $('#js-form')[0].reset();
                          }
                      });
                  });
              </script>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-warning">
              <div class="card-header">
                <h5 class="card-title">Последнее прочитанное</h5>
              </div>
              <!-- /.card-header -->
                <div class="card-body">
                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item"><a class="">Математика</a><p class="float-right">174 стр.</p></li>
                        <li class="list-group-item"><a class="">Русский язык</a><p class="float-right">135 стр.</p></li>
                        <li class="list-group-item"><a class="">Биология</a><p class="float-right">121 стр.</p></li>
                        <li class="list-group-item"><a class="">Англ. язык</a><p class="float-right">24 стр.</p></li>

                    </ul>
                </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Записи</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Настройки профиля</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="post">
                      <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="dist/img/user1-128x128.jpg" alt="user image">
                        <span class="username">
                          <a href="#">Николай Павленко</a>
                          <a href="#" class="float-right btn-tool"><i class="fa fa-times"></i></a>
                        </span>
                      </div>
                      <!-- /.user-block -->
                      <p>
                        сообщение
                      </p>

                      <p>
                        <a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up mr-1"></i>Лайк</a>
                        <span class="float-right">
                          <a href="#" class="link-black text-sm">
                            <i class="fa fa-comments-o mr-1"></i> Комментарии (5)
                          </a>
                        </span>
                      </p>

                      <input class="form-control form-control-sm" type="text" placeholder="Оставте комментарий">
                    </div>
                    <!-- /.post -->
                  </div>
                  <!-- /.tab-pane -->


                  <div class="tab-pane" id="settings">
                    <form class="form-horizontal">
                      <div class="form-group">
                        <label for="oldPassword" class="col-sm-10 control-label">Пароль</label>

                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword" class="col-sm-10 control-label">Новый пароль</label>

                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="inputPassword" placeholder="New password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword2" class="col-sm-10 control-label">Повторите пароль</label>

                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputPassword2" placeholder="Name">
                        </div>
                      </div>
                        <button class="btn-warning btn" style="position:relative; left: 8px;">Сменить пароль</button>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<?php require ('footer.php');?>
