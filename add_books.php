<?php
require ('config.php');

require ('header.php');
// Название <input type="file">
$input_name = 'file';

// Разрешенные расширения файлов.
$allow = array();

// Запрещенные расширения файлов.
$deny = array(
    'phtml', 'php', 'php3', 'php4', 'php5', 'php6', 'php7', 'phps', 'cgi', 'pl', 'asp',
    'aspx', 'shtml', 'shtm', 'htaccess', 'htpasswd', 'ini', 'log', 'sh', 'js',
    'htm', 'css', 'sql', 'spl', 'scgi', 'fcgi'
);

// Директория куда будут загружаться файлы.
$path = __DIR__ . '/upload/book/';

if (isset($_FILES[$input_name])) {
    // Проверим директорию для загрузки.
    if (!is_dir($path)) {
        mkdir($path, 0777, true);
    }

    // Преобразуем массив $_FILES в удобный вид для перебора в foreach.
    $files = array();
    $diff = count($_FILES[$input_name]) - count($_FILES[$input_name], COUNT_RECURSIVE);
    if ($diff == 0) {
        $files = array($_FILES[$input_name]);
    } else {
        foreach($_FILES[$input_name] as $k => $l) {
            foreach($l as $i => $v) {
                $files[$i][$k] = $v;
            }
        }
    }

    foreach ($files as $file) {
        $error = $success = '';

        // Проверим на ошибки загрузки.
        if (!empty($file['error']) || empty($file['tmp_name'])) {
            switch (@$file['error']) {
                case 1:
                case 2: $error = 'Превышен размер загружаемого файла.'; break;
                case 3: $error = 'Файл был получен только частично.'; break;
                case 4: $error = 'Файл не был загружен.'; break;
                case 6: $error = 'Файл не загружен - отсутствует временная директория.'; break;
                case 7: $error = 'Не удалось записать файл на диск.'; break;
                case 8: $error = 'PHP-расширение остановило загрузку файла.'; break;
                case 9: $error = 'Файл не был загружен - директория не существует.'; break;
                case 10: $error = 'Превышен максимально допустимый размер файла.'; break;
                case 11: $error = 'Данный тип файла запрещен.'; break;
                case 12: $error = 'Ошибка при копировании файла.'; break;
                default: $error = 'Файл не был загружен - неизвестная ошибка.'; break;
            }
        } elseif ($file['tmp_name'] == 'none' || !is_uploaded_file($file['tmp_name'])) {
            $error = 'Не удалось загрузить файл.';
        } else {
            // Оставляем в имени файла только буквы, цифры и некоторые символы.
            $pattern = "[^a-zа-яё0-9,~!@#%^-_\$\?\(\)\{\}\[\]\.]";
            $name = mb_eregi_replace($pattern, '-', $file['name']);
            $name = mb_ereg_replace('[-]+', '-', $name);

            // Т.к. есть проблема с кириллицей в названиях файлов (файлы становятся недоступны).
            // Сделаем их транслит:
            $converter = array(
                'а' => 'a',   'б' => 'b',   'в' => 'v',    'г' => 'g',   'д' => 'd',   'е' => 'e',
                'ё' => 'e',   'ж' => 'zh',  'з' => 'z',    'и' => 'i',   'й' => 'y',   'к' => 'k',
                'л' => 'l',   'м' => 'm',   'н' => 'n',    'о' => 'o',   'п' => 'p',   'р' => 'r',
                'с' => 's',   'т' => 't',   'у' => 'u',    'ф' => 'f',   'х' => 'h',   'ц' => 'c',
                'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',  'ь' => '',    'ы' => 'y',   'ъ' => '',
                'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

                'А' => 'A',   'Б' => 'B',   'В' => 'V',    'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
                'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',    'И' => 'I',   'Й' => 'Y',   'К' => 'K',
                'Л' => 'L',   'М' => 'M',   'Н' => 'N',    'О' => 'O',   'П' => 'P',   'Р' => 'R',
                'С' => 'S',   'Т' => 'T',   'У' => 'U',    'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
                'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',  'Ь' => '',    'Ы' => 'Y',   'Ъ' => '',
                'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
            );

            $name = strtr($name, $converter);
            $parts = pathinfo($name);

            if (empty($name) || empty($parts['extension'])) {
                $error = 'Недопустимое тип файла';
            } elseif (!empty($allow) && !in_array(strtolower($parts['extension']), $allow)) {
                $error = 'Недопустимый тип файла';
            } elseif (!empty($deny) && in_array(strtolower($parts['extension']), $deny)) {
                $error = 'Недопустимый тип файла';
            } else {
                // Чтобы не затереть файл с таким же названием, добавим префикс.
                $i = 0;
                $prefix = '';
                while (is_file($path . $parts['filename'] . $prefix . '.' . $parts['extension'])) {
                    $prefix = '(' . ++$i . ')';
                }
                $name = $parts['filename'] . $prefix . '.' . $parts['extension'];
                $success = 'Файл «' . $name . '» успешно загружен.';
                $book=$_POST['name_book'];
                $text=$_POST['text_book'];
                $class=$_POST['class'];
                // Перемещаем файл в директорию.
                if (($_POST['sent']!='')&&(move_uploaded_file($file['tmp_name'], $path . $name))) {
                    // Далее можно сохранить название файла в БД и т.п.
                        $sql="INSERT INTO `books` (`name_book`, `text_book`, `class_book`, `href`) VALUES ('$book', '$text', '$class', '$name')";
                        $pdo->query($sql);
                } else {
                    $error = 'Не удалось загрузить файл.';
                }
            }
        }
    }
}
require ('sidebar.php');
?>


<div class="content-wrapper">
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-9">
                <div class="table-add-user">
                    <form method="POST" id="js-form" enctype="multipart/form-data">
                        <label for="">Введите название книги:</label>
                        <input name='name_book' class="form-control">
                        <hr>
                        <label for="">Выберите класс:</label>
                        <select name='class' class="form-control">
                            <option value='1'>1 класс</option>
                            <option value='2'>2 класс</option>
                            <option value='3'>3 класс</option>
                            <option value='4'>4 класс</option>
                            <option value='5'>5 класс</option>
                            <option value='6'>6 класс</option>
                            <option value='7'>7 класс</option>
                            <option value='8'>8 класс</option>
                            <option value='9'>9 класс</option>
                            <option value='10'>10 класс</option>
                            <option value='11'>11 класс</option>
                        </select>
                        <div class="card card-outline card-info" style="margin-top: 15px;">
                            <!-- /.card-header -->
                            <div class="card-body pad">
                                <div class="mb-3">
                                   <textarea id="summernote" name="text_book" placeholder="Введите описанике книги" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                </div>
                            </div>
                        </div>
                        <input id="js-file" type="file" name="file[]" multiple>
                        <button type="submit" class="btn btn-primary" name='sent' value='book.php?id='$row['id']'>Добавить</button>
                    </form>
                    <div id="result">
                        <?php
                        if (!empty($success)) {
                            echo '<p>' . $success . '</p>';
                        } else {
                            echo '<p>' . $error . '</p>';
                        }
                        ?>
                    </div>
                    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                    <script src="//malsup.github.io/min/jquery.form.min.js"></script>

                    <script>
                        $('#js-file').change(function() {
                            $('#js-form').ajaxSubmit({
                                type: 'POST',
                                url: '/add_books.php',
                                target: '#result',
                                success: function() {
                                    // После загрузки файла очистим форму.
                                    $('#js-form')[0].reset();
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<?= require ('footer.php') ?>
