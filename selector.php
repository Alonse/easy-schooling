<? 
header("Content-type: application/json");

include_once ('config.php');

if(empty($_SESSION['id'])) {
    http_response_code(404);
    echo json_encode(["err"=>"Auth required"]);
    exit();

}

$sel = file_get_contents('php://input');
$user = intval($_SESSION['id']);
$book = intval($_GET['book']);

$bookCheck = $pdo->query("SELECT books.id FROM books INNER JOIN users ON users.id=$user AND books.class_book = users.class WHERE books.id=$book");

if ($bookCheck->rowCount() != 1 && !isset($_GET['reply'])) {
    http_response_code(404);
    echo json_encode(["err"=>"Needed book not found!"]);
    exit();
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    
    if (!empty($_GET['scroll'])) {
        $pdo->query("INSERT INTO selections SET type='scroll', userid=$user, bookid=$book, data='".intval($_GET['scroll'])."' ON DUPLICATE KEY UPDATE data='".intval($_GET['scroll'])."'");
    } elseif ( isset($_GET['comment']) ) {

        $sel = json_decode($sel, false);

        $list = $pdo->query(
            "SELECT id FROM selections
            WHERE 
                type='sel'
                AND userid=$user AND bookid=$book
                AND startKey=" . intval($sel->selection->startKey) . "
                AND startTextIndex=" . intval($sel->selection->startTextIndex) . "
                AND startOffset=" . intval($sel->selection->startOffset) . "
            LIMIT 1
            ");
    
        $list = $list->fetchAll();
        $item = empty($list[0]['id']) ? 0 : $list[0]['id'];
            
    
        if ($item > 0) {
            $teacher = isset($sel->teacher) ? intval($sel->teacher) : 0;

            $pdo->query("UPDATE selections set data='" . base64_encode($sel->comment) . "', teacherid=$teacher WHERE id=$item");
            echo json_encode(["status"=>200,'data'=>$sel,'id'=>$currentSel]);
        } else {
            echo json_encode(["status"=>404,'data'=>$sel]);
        }
    } elseif (isset($_GET['reply'])) {
        if ($_SESSION['role'] == 'teacher') {
            $sel = base64_encode($sel);
            $for = intval($_GET['for']);

            $pdo->query("UPDATE selections SET data='$sel' WHERE id=$for AND teacherid=$user");
        } else {
            echo json_encode(["err"=>"Role auth required"]);
        }
    } else {
        $sel = json_decode($sel, false);
        foreach ($sel as $i=>$s) {
            foreach($s as $field=>$value) $s->$field = intval($value);

            $pdo->query("INSERT INTO selections 
                (type, userid, bookid, startKey, startTextIndex, startOffset, endKey, endTextIndex, endOffset)
                VALUES('sel', $user, $book, $s->startKey, $s->startTextIndex, $s->startOffset, $s->endKey, $s->endTextIndex, $s->endOffset)
                ");
        }

    }
}elseif ($_SERVER['REQUEST_METHOD'] == "GET") {
    $response = [];

    if (isset($_GET['list'])) {
        if ($_SESSION['role'] == 'teacher') {
            $pId = isset($_GET['for']) ? intval($_GET['for']) : 0;

            $queryString = "SELECT selections.*, users.id as uid, CONCAT(users.name, ' ', users.surname) as user 
            FROM selections 
            LEFT JOIN users on users.id=selections.userid
            WHERE teacherid=$user AND bookid=$book " . ($pId == 0 ? '' : " AND startKey=$pId") . "
            ORDER BY id ASC";

            $list = $pdo->query($queryString);

        } elseif($_SESSION['role'] == 'student') {
            $list = $pdo->query(
                "SELECT * 
                FROM selections 
                WHERE userid=$user AND bookid=$book 
                ORDER BY id ASC
                ");
        }

        while ($row=$list->fetch()) {
            if ($row['type'] == 'scroll') {
                $response["scroll"] = intval($row['data']);
            } elseif ($row['type'] == 'sel' || $row['type']=="hsel") {
                $fields = ["startKey", "startTextIndex", "startOffset", "endKey", "endTextIndex", "endOffset"];
                foreach($fields as $fieldName) {
                    $row['obj'][$fieldName] = $row[$fieldName];
                }

                $id = md5(json_encode($row['obj']) . $user . $book);

                $response[$id]['sel'] = $row['obj'];
                $response[$id]["comment"] = base64_decode($row["data"]);

                if ($row['type'] == 'hsel')
                    $response[$id]['hidden']=true;
    
                if (!empty($row["teacherid"])) {
                    $response[$id]["teacher"] = intval($row["teacherid"]);
                }
                if ($_SESSION['role'] == 'teacher') {
                    $response[$id]["from"] = [
                        'id' => $row['uid'],
                        "name" => $row['user']
                    ];
                    $response[$id]["id"] = $row['id'];
                }

            }
        }
    } elseif (isset($_GET['teacherslist'])) {
        $list = $pdo->query(
            "SELECT DISTINCT teacher.id,teacher.name,teacher.surname 
            FROM users as teacher 
            LEFT JOIN users as student 
                ON student.class=teacher.class AND student.school=teacher.school AND student.id=$user
                WHERE teacher.role='teacher'"
        );

        while ($row=$list->fetch()) {
            $response[$row["id"]] = $row["name"] . " " . $row["surname"];
        }
    } elseif (isset($_GET['glist'])) {
        $list = $pdo->query(
            "SELECT 
                sel.*,
                CONCAT(users.name, ' ', users.surname) as teacher
            FROM selections as sel
            left join users
                on users.id = sel.teacherid
            WHERE sel.userid=$user AND sel.bookid=$book and sel.type = 'sel'
            ORDER BY sel.timestamp DESC
            ");

        while ($row=$list->fetch()) {
            $fields = ["startKey", "startTextIndex", "startOffset", "endKey", "endTextIndex", "endOffset"];
            foreach($fields as $fieldName) {
                $row['obj'][$fieldName] = $row[$fieldName];
            }

            $id = md5(json_encode($row['obj']) . $user . $book);

            $response[$id] = [
                "comment" => base64_decode($row["data"]),
                "tid" => $row['teacherid'],
                "tname" => $row['teacher']
            ];
        }
    }
    
    echo json_encode($response);

    exit();
} elseif ($_SERVER['REQUEST_METHOD'] == "DELETE") {
    $sel = json_decode($sel, false);
    $list = $pdo->query(
        "SELECT id FROM selections
        WHERE 
            userid=$user AND bookid=$book
            AND startKey=" . intval($sel->startKey) . "
            AND startTextIndex=" . intval($sel->startTextIndex) . "
            AND startOffset=" . intval($sel->startOffset) . "
        LIMIT 1
        ");

    $list = $list->fetchAll();
    $toDelete = empty($list[0]['id']) ? 0 : $list[0]['id'];

    if ($toDelete > 0) {
        $pdo->query("UPDATE selections SET type='hsel' WHERE id=$toDelete");
        echo json_encode(["status"=>200,'data'=>$sel]);
    } else {
        echo json_encode(["status"=>404,'data'=>$sel]);
    }
}

?>
