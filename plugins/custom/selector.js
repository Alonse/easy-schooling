scrollPos = {
  current: 0,
  server: 0
}

function rangeToObj(range) {
    return {
      startKey: range.startContainer.parentNode.dataset.key,
      startTextIndex: Array.prototype.indexOf.call(range.startContainer.parentNode.childNodes, range.startContainer),
      endKey: range.endContainer.parentNode.dataset.key,
      endTextIndex: Array.prototype.indexOf.call(range.endContainer.parentNode.childNodes, range.endContainer),
      startOffset: range.startOffset,
      endOffset: range.endOffset
    }
}

function objToRange(rangeStr) {
  range = document.createRange();
  range.setStart(document.querySelector('[data-key="' + rangeStr.startKey + '"]').childNodes[rangeStr.startTextIndex], rangeStr.startOffset);
  range.setEnd(document.querySelector('[data-key="' + rangeStr.endKey + '"]').childNodes[rangeStr.endTextIndex], rangeStr.endOffset);
  return range;
}

function hlRange(range, id, data) {
  var span = document.createElement('span');

  if (!data || !data.hidden) {
    span.className = 'highlight';
    if (id)span.id = id;
    if (data) {
      if (data.comment)span.dataset.comment = data.comment;
      if (data.teacher)span.dataset.teacher = data.teacher;
    }
  } else {
    span.className = 'hlHidden';
  }

  span.appendChild(range.extractContents());
  span.dataset.selection = JSON.stringify(rangeToObj(range))

  range.insertNode(span);

  if (!data || !data.hidden) {
    $(span).mouseenter(function(In){
        editingSpan = In.target;

        $("#hMenu").css({
          top: (In.target.offsetTop + In.target.offsetHeight-5)+'px', 
          left: In.target.offsetLeft+'px'}
        );
        //$("#hMenu")[0].dataset.selection = ;
        //$("#hMenu")[0].dataset.id = span.id;
        
        $("#hMenu").show(200);

        $("#hMenu #hDelete").click(function(ev){
          $.ajax({
            method: "DELETE",
            url: "/selector.php?user=" + userId + "&book=" + bookId,
            data: editingSpan.dataset.selection,
            success: function(data) {
              if (data.status == 200) {
                $(span).removeClass('highlight');
                $(span).addClass('hlHidden');
                $(span).unbind('mouseenter');
              }
            }
          });
          hideHMenu();
        })

        $("#hMenu #hComment").click(function(ev){
          $("#hCommentContentWrapper").show(100);

          $("#hCommentContentWrapper .edContent .content")[0].dataset.selection = editingSpan.dataset.selection
          $("#hCommentContentWrapper .edContent .content")[0].dataset.id = editingSpan.dataset.id

          if (editingSpan.dataset.teacher)
            $(".edContent select#teacherSelect").val(editingSpan.dataset.teacher)
          
          if (span.dataset.comment) 
            $("#hCommentContentWrapper .edContent .content").html(span.dataset.comment)
          else
            $("#hCommentContentWrapper .edContent .content").html("")

          $("#hCommentContentWrapper").click(function(ev){
            if ($("#hCommentContentWrapper").has(ev.target).length) return;

            selectionData = JSON.parse($("#hCommentContentWrapper .edContent .content")[0].dataset.selection)

            $.post(
              "/selector.php?comment&user=" + userId + "&book=" + bookId, 
              JSON.stringify({
                selection: selectionData,
                comment: $("#hCommentContentWrapper .edContent .content").html(),
                teacher: $(".edContent select#teacherSelect").val()
              })
            );
            
            span.dataset.comment = $("#hCommentContentWrapper .edContent .content").html();
            span.dataset.teacher = $(".edContent select#teacherSelect").val()
            $(".edContent select#teacherSelect").val(0)

            $("#hCommentContentWrapper").hide(100);
            $("#hCommentContentWrapper").unbind();
            $("#hCommentContentWrapper")[0].dataset.id = '';
            editingSpan = undefined;
          })

          $("#hCommentContentWrapper .edContent *").keydown(function(ev){
            if (ev.which === 9) ev.preventDefault();
          })

          hideHMenu();
        })

    });
  }
}

function hideHMenu() {
  $("#hMenu").hide(200);
  //$("#hMenu")[0].dataset.selection = ''
  //$("#hMenu")[0].dataset.id = ''
  $("#hMenu div").unbind('click');
  editingSpan = undefined;
}
$(document).mousemove(function(Out) {
  var target = $(Out.target);
  if ($("#hMenu").is(":visible")) {
    if (target.parent("#hMenu").length || target[0].id == "hMenu" || target.hasClass("highlight")) {
      return
    } else {
      hideHMenu();
    }
  }
});

$(document).click(function(e){
  if (e.target.id != "slContext") {
    $("#slContext").hide(200);
    $("#hmMenu").hide(200);
    $("#tMenu").hide(200);
  }

  if (e.target.id == "bookHamburger" || $(e.target).parent("#bookHamburger").length) {
    $("#hmMenu").html("");
    $.get("/selector.php?glist&user=" + userId + "&book=" + bookId, function(data) {
      if(data.length == 0) return;
      for(var id in data){
        var div = document.createElement('div');

        div.dataset.id = id;
        div.className = "hmItem linkLike"
        var text = $("span#"+id).text();
        if (text.length > 15) text = text.substr(0,15)+"..."

        div.innerHTML = "<div>#" + id.substr(0,4) + " " + text + "</div>"
        if (data[id].comment) div.innerHTML += "<div>Comment: " + data[id].comment + "</div>";
        if (data[id].tname) div.innerHTML += "<div data-to='"+data[id].tid+"'>To: " + data[id].tname + "</div>";

        $(div).click(function(ev){
          var target = ev.target;
          while(!target.dataset.id) target = target.parentNode;

          try{
            $([document.documentElement, document.body]).animate({
              scrollTop: $("#"+target.dataset.id).offset().top-30
            }, 200);
          } catch(e) {}
          
        });

        $("#hmMenu").append(div);

      };
      $("#hmMenu").show(200);
    });
  }
})

document.addEventListener('contextmenu', function(e) {
    if ($("#sys").has(e.target).length) return;

    if (e.target.className == "hlHidden") {
      
    } else {
      sel = document.getSelection();
    }

    if(sel.rangeCount == 0)return;
    if(sel.toString() == "") return;

    e.preventDefault();

    selData = []
    for (var i=0; i<sel.rangeCount; i++) {
        var range = sel.getRangeAt(i);

        selData.push(rangeToObj(range));
        hlRange(range);
    }
    $(".highlight *").not("highlight").addClass('highlight');
    
    var data = JSON.stringify(selData);
    $.post("/selector.php?user=" + userId + "&book=" + bookId, data);

    //$("#slContext").text().show(200);

    document.getSelection().removeAllRanges();
}, false);

$(document).scroll(function(ev){
  scrollPos.current = $(document).scrollTop();
})

window.setInterval(function(){
  if (scrollPos.current != scrollPos.server) {
    scrollPos.server = scrollPos.current;
    $.post("/selector.php?user=" + userId + "&book=" + bookId + "&scroll=" + scrollPos.current);
  }
}, 3000);

$(document).ready(function() {
  childIndex = 0;

  function addKey(element) {
      if (element.children.length > 0) {
        Array.prototype.forEach.call(element.children, function(each, i) {
          each.dataset.key = childIndex++;
          addKey(each)
        });
      }
    };
    
  addKey($("content")[0])

  $.get("/selector.php?list&user=" + userId + "&book=" + bookId, function(data) {
    for(var objId in data){
      if (objId == "scroll") {
        $(document).scrollTop(data['scroll']);
        scrollPos.current = data['scroll'];
        scrollPos.server = data['scroll'];
        continue;
      } else {
        try {
          if (data[objId]['from']) {
            var el = $("[data-key="+data[objId]['sel']['startKey']+"]");
            if (el.hasClass('highlight')) continue;

            el.addClass('highlight')

            el.contextmenu(function(ev){
              ev.preventDefault();

              fireEvent = ev;
              $.get("/selector.php?list&for="+ev.target.dataset.key+"&user=" + userId + "&book=" + bookId, function(data) {
                if (data.length == 0) {
                  $(ev.target).unbind();
                  $(ev.target).removeClass('highlight');
                  return
                }
                
                var menu = $("#tMenu");
                menu.html("")

                menu.css({
                  top: (fireEvent.originalEvent.pageY-5)+'px', 
                  left: fireEvent.originalEvent.pageX+'px'
                })

                menu.show(200);
                for (id in data) {
                  var div = document.createElement('div');
                  div.id = data[id]['id'];
                  div.className = 'linkLike'
                  div.innerHTML = 'From: ' + data[id]['from']['name'] + '<br>' + data[id]['comment'].substr(0,10) + '...';
                  div.dataset.data = JSON.stringify(data[id])

                  menu.append(div);

                  $(div).on('click', function(ev){

                    var data = JSON.parse(ev.target.dataset.data)

                    $("#hCommentContentWrapper").show(100);
                    $("select#teacherSelect").hide(0);
                    $(".edContent .selector #student").remove();
                    $(".edContent .selector").append('<span id="student">From: ' + data['from']['name']+ "</span>")
                    $(".edContent .content").html(data['comment'])

                    $("#hCommentContentWrapper").click(function(ev){
                      if ($("#hCommentContentWrapper").has(ev.target).length) return;

                      $.ajax({
                        type: 'POST',
                        url: "/selector.php?reply&for=" + data['id'],
                        data: $(".edContent .content").html(),
                        dataType: 'string'
                      })
          
                      $(".edContent .content").html("")
                      $(".edContent .selector #student").remove();
                      $("select#teacherSelect").show(0);

                      $("#hCommentContentWrapper").hide(100);
                      $("#hCommentContentWrapper").unbind();
                    
                      editingSpan = undefined;
                    })
          
                    $("#hCommentContentWrapper .edContent *").keydown(function(ev){
                      if (ev.which === 9) ev.preventDefault();
                    })
          
                    hideHMenu();
          

                  })
                }  

              })
            })
          } else {
            hlRange(objToRange(data[objId]['sel']), objId, data[objId]);
          }
        } catch(e) {
          console.log("Something went wrong - maybe selection-in-selection")
          console.log(e);
        }
  }
    }
  });

  $.get("/selector.php?teacherslist&user=" + userId + "&book=" + bookId, function(data){
    $(".edContent select#teacherSelect").append(new Option("None", 0));
    for(var id in data){
      $(".edContent select#teacherSelect").append(new Option(data[id], id));
    }
  });

})
