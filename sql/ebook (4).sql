-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 14, 2019 at 12:48 PM
-- Server version: 5.5.62-0ubuntu0.14.04.1
-- PHP Version: 7.2.14-1+ubuntu14.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebook`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `name_book` varchar(300) NOT NULL,
  `text_book` longtext NOT NULL,
  `class_book` varchar(255) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '1',
  `href` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `books`
--

TRUNCATE TABLE `books`;
--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `name_book`, `text_book`, `class_book`, `view`, `href`) VALUES
(30, 'Информатика', '', '6', 1, 'informatika_6kl_Makarova_rus_2018.html'),
(31, 'Бел. язык', '', '1', 1, 'bel_mova_Antonova_1kl_rus_2017.html'),
(33, 'Английский язык', '', '1', 1, 'english-4kl-ch2-rus.html'),
(34, 'Алгебра', '', '7', 1, 'Algebra_Arefieva_7_bel_2017.html'),
(35, 'Информатика', '', '1', 1, 'Algebra_Arefieva_7_bel_2017.tcr'),
(36, 'Критическая масса', '', '1', 1, 'crit_m.html');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `image`
--

TRUNCATE TABLE `image`;
--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `img`) VALUES
(1, 'diff4.png');

-- --------------------------------------------------------

--
-- Table structure for table `selections`
--

DROP TABLE IF EXISTS `selections`;
CREATE TABLE `selections` (
  `id` int(11) NOT NULL,
  `obj` text COLLATE utf8_bin NOT NULL,
  `userid` int(11) NOT NULL,
  `bookid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Truncate table before insert `selections`
--

TRUNCATE TABLE `selections`;
--
-- Dumping data for table `selections`
--

INSERT INTO `selections` (`id`, `obj`, `userid`, `bookid`) VALUES
(20, 'W3sic3RhcnRLZXkiOiIyIiwic3RhcnRUZXh0SW5kZXgiOjE0LCJlbmRLZXkiOiIyIiwiZW5kVGV4dEluZGV4IjoxNCwic3RhcnRPZmZzZXQiOjM0LCJlbmRPZmZzZXQiOjQ4fV0=', 680, 36);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `school` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `city`, `school`, `role`, `class`, `subject`, `login`, `password`, `image`) VALUES
(678, 'Николай', 'Павленко', 'Микашевичи', '13', 'student', '11', ' ', 'I_Ivanov', '123', ''),
(679, 'Александр', 'Пушкин', 'Минск', '13', 'student', '1', ' ', 'I_Sanya', '12345678', 'obez07(3).gif'),
(680, 'Николай', 'Павленко', 'Микашевичи', '21', 'student', '1', ' ', 'nikolay', '12345', 'obez07(1)(1).gif'),
(682, 'Саня', 'Петров', 'Минск', '1', 'student', '11', ' ', 'sasa', '12345678', ''),
(683, 'Андрей', 'Суперадмин', 'Марс', '13', 'student', '7', ' ', 'A_Superadmin', '123', 'KsY6.gif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selections`
--
ALTER TABLE `selections`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user's book selection` (`userid`,`obj`(200),`bookid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `selections`
--
ALTER TABLE `selections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=684;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
