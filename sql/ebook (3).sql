-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 12 2019 г., 16:17
-- Версия сервера: 5.7.23
-- Версия PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `ebook`
--

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `name_book` varchar(300) NOT NULL,
  `text_book` longtext NOT NULL,
  `class_book` varchar(255) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '1',
  `href` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`id`, `name_book`, `text_book`, `class_book`, `view`, `href`) VALUES
(30, 'Информатика', '', '6', 1, 'informatika_6kl_Makarova_rus_2018.html'),
(31, 'Бел. язык', '', '1', 1, 'bel_mova_Antonova_1kl_rus_2017.html'),
(33, 'Английский язык', '', '1', 1, 'english-4kl-ch2-rus.html'),
(34, 'Алгебра', '', '7', 1, 'Algebra_Arefieva_7_bel_2017.html'),
(35, 'Информатика', '', '1', 1, 'Algebra_Arefieva_7_bel_2017.tcr');

-- --------------------------------------------------------

--
-- Структура таблицы `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `image`
--

INSERT INTO `image` (`id`, `img`) VALUES
(1, 'diff4.png');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `school` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `city`, `school`, `role`, `class`, `subject`, `login`, `password`, `image`) VALUES
(678, 'Николай', 'Павленко', 'Микашевичи', '13', 'student', '11', ' ', 'I_Ivanov', '123', ''),
(679, 'Александр', 'Пушкин', 'Минск', '13', 'student', '1', ' ', 'I_Sanya', '12345678', 'obez07(3).gif'),
(680, 'Николай', 'Павленко', 'Микашевичи', '21', 'student', '1', ' ', 'nikolay', '12345', 'obez07(1)(1).gif'),
(682, 'Саня', 'Петров', 'Минск', '1', 'student', '11', ' ', 'sasa', '12345678', ''),
(683, 'Андрей', 'Суперадмин', 'Марс', '13', 'student', '7', ' ', 'A_Superadmin', '123', 'KsY6.gif');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=684;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
