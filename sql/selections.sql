-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 19, 2019 at 10:56 AM
-- Server version: 5.5.62-0ubuntu0.14.04.1
-- PHP Version: 7.2.14-1+ubuntu14.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebook`
--

-- --------------------------------------------------------

--
-- Table structure for table `selections`
--

DROP TABLE IF EXISTS `selections`;
CREATE TABLE `selections` (
  `id` int(11) NOT NULL,
  `type` enum('sel','scroll','hsel') COLLATE utf8_bin NOT NULL,
  `startKey` int(11) NOT NULL,
  `startTextIndex` int(11) NOT NULL,
  `startOffset` int(11) NOT NULL,
  `endKey` int(11) NOT NULL,
  `endTextIndex` int(11) NOT NULL,
  `endOffset` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `bookid` int(11) NOT NULL,
  `teacherid` int(11) DEFAULT NULL,
  `data` text COLLATE utf8_bin,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `selections`
--

INSERT INTO `selections` (`id`, `type`, `startKey`, `startTextIndex`, `startOffset`, `endKey`, `endTextIndex`, `endOffset`, `userid`, `bookid`, `teacherid`, `data`, `timestamp`) VALUES
(73, 'scroll', 0, 0, 0, 0, 0, 0, 680, 36, NULL, '401', '2019-03-19 11:25:13'),
(88, 'sel', 8, 0, 407, 8, 0, 428, 680, 36, 684, 'LS0tLS0t', '2019-03-19 12:45:03'),
(89, 'sel', 8, 2, 380, 8, 2, 387, 680, 36, 684, 'dGV4dA==', '2019-03-19 12:45:33'),
(90, 'scroll', 0, 0, 0, 0, 0, 0, 684, 36, NULL, '128', '2019-03-19 13:09:54'),
(92, 'sel', 9, 0, 858, 9, 0, 873, 680, 36, 684, '', '2019-03-19 13:15:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `selections`
--
ALTER TABLE `selections`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unic` (`type`,`startKey`,`startTextIndex`,`startOffset`,`endKey`,`endTextIndex`,`endOffset`,`userid`,`bookid`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `selections`
--
ALTER TABLE `selections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
