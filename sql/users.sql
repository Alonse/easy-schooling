-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 19, 2019 at 10:57 AM
-- Server version: 5.5.62-0ubuntu0.14.04.1
-- PHP Version: 7.2.14-1+ubuntu14.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebook`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `school` varchar(255) NOT NULL,
  `role` enum('student','admin','teacher','') NOT NULL,
  `class` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `city`, `school`, `role`, `class`, `subject`, `login`, `password`, `image`) VALUES
(678, 'Николай', 'Павленко', 'Микашевичи', '13', 'student', '11', ' ', 'I_Ivanov', '123', ''),
(679, 'Александр', 'Пушкин', 'Минск', '13', 'student', '1', ' ', 'I_Sanya', '12345678', 'obez07(3).gif'),
(680, 'Николай', 'Павленко', 'Микашевичи', '21', 'student', '1', ' ', 'nikolay', '12345', 'obez07(1)(1).gif'),
(682, 'Саня', 'Петров', 'Минск', '1', 'student', '11', ' ', 'sasa', '12345678', ''),
(683, 'Андрей', 'Суперадмин', 'Марс', '21', 'student', '1', ' ', 'A_Superadmin', '123', 'KsY6.gif'),
(684, 'Иван', 'Иванович', 'Минск', '21', 'teacher', '1', '', 'iviv', '12345', ''),
(685, 'Петр', 'Петрович', 'Минск', '21', 'teacher', '1', '', 'iviviv', '12345', ''),
(686, 'Сидор', 'Сидрович', 'Минск', '21', 'teacher', '1', '', 'iviviviv', '12345', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=687;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
