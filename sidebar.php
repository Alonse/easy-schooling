<?php
include ('config.php');
$result=$pdo->query("SELECT * FROM `users`");
$row=$result->fetch();

if($_POST['exit'] == 'ok'){
    unset($_SESSION);
    header("location: /auth.php");
};
?>

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
        <a href="#" class="brand-link">
            <span class="brand-text font-weight-light"><strong style="color: orange; font-family: 'Comic Sans MS', Helvetica, Arial, sans-serif font-weight: 600;">E</strong>-Class</span>
        </a>
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <?php
                $image=$_SESSION['image'];
                echo '<img src="upload/avatar/'.$image.'" class="img-circle elevation-2" alt="User Image">';
                ?>
            </div>
            <div class="info">
                <?php
                echo '<a href="profile.php">'.$_SESSION['name'].' '.$_SESSION['surname'].'</a>'
                ?>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2 sidebar-mt">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-item">
                    <a href="index.php" class="nav-link">
                        <i class="fa fa-book nav-icon"></i>
                        <p>Учебники</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="video.php" class="nav-link">
                        <i class="fa fa-video-camera nav-icon"></i>
                        <p>Видео</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fa fa-file-audio-o nav-icon"></i>
                        <p>Аудио</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fa fa-bookmark-o nav-icon"></i>
                        <p>Доп. материалы</p>
                    </a>
                </li>
            </ul>
        </nav>

        <!-- /.sidebar-menu -->
    </div>

    <!-- /.sidebar -->
</aside>
