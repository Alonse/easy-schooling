<?php
session_start();
require('config.php');
if($_SESSION['login'] ==''){
    header("location: /auth.php");
};

if($_POST['exit'] == 'ok'){
    unset($_SESSION['login']);
    header("location: /auth.php");
};
?>

<?= require ('header.php'); ?>

<?= include ('sidebar.php');?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <section class="content-header">

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Список учеников</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="" method="GET">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Имя</th>
                                        <th>Фамилия</th>
                                        <th>Школа</th>
                                        <th>Класс</th>
                                        <th>Город</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <?php
                                        $result=$pdo->query("SELECT * FROM `users`");
                                        while ($row=$result->fetch())
                                        {
                                            echo '<tr><td>'.$row['name'].'</td><td>'.$row['surname'].'</td><td>'.$row['school'].'</td><td>'.$row['class'].'</td><td>'.$row['city'].'</td></tr>';
                                        }
                                        ?>
                                    </tr>
                                    </tfoot>
                                </table>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
</div>

<?= require ('footer.php'); ?>
