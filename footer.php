<?php

?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Версия</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2019 <a href="#">E-book Studio</a>.</strong> Все права защищены
</footer>

<aside class="control-sidebar control-sidebar-dark">
    <div class="sidebar">
        <nav class="mt-2 sidebar-mt">
            <ul class="nav nav-pills nav-sidebar flex-column">
                <?php
                if($_SESSION['role'] == 'admin') {
                    echo '<li class="nav-item">';
                    echo '<a href = "table_user.php" class="nav-link"><i class="fa fa-bookmark-o nav-icon"></i> <p>Список пользователей</p></a >';
                    echo '</li>';
                    echo '<li class="nav-item">';
                    echo '<a href = "add_user.php" class="nav-link" ><i class="fa fa-bookmark-o nav-icon"></i><p>Добавить Ученика</p></a >';
                    echo '</li>';
                    echo '<li class="nav-item">';
                    echo '<a href = "add_books.php" class="nav-link" ><i class="fa fa-bookmark-o nav-icon"></i><p>Добавить Учебник</p></a >';
                    echo '</li >';
                } ?>
            </ul>
        </nav>
        <div class="bottom">
            <form action="" method="POST">
                <button type="submit" value="ok" name="exit" class="btn btn-warning" style="position: relative; left: 10%; width: 50%;">Выход</button>
            </form>
        </div>
    </div>
</aside>
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>

<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<!--<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>-->
<!--<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>-->
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="dist/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</div>
</body>
</html>
