<? 
include_once ('config.php');

if($_SESSION['login'] ==''){
    header("location: /auth.php");
};

if (isset($_GET['content'])) {
    $bookId = intval($_GET['id']);
    $userId = intval($_SESSION['id']);

    ?>
        <head>
            <script src="plugins/jquery/jquery.min.js"></script>
            <script src="plugins/jquery/isInViewport.min.js"></script>
            <script src="plugins/custom/selector.js"></script>
            <link rel="stylesheet" type="text/css" href="plugins/custom/selector.css">
            <script>
                bookId = <?=$bookId?>;
                userId = <?=$userId?>;
            </script>
        </head>
    <?
    $result=$pdo->query("SELECT href FROM `books` where id = " . intval($_GET['id']));
    $book = $result->fetch();
    $content = file_get_contents(__DIR__ . '/upload/book/' . $book['href']);
    ?>
    <body id="book">
        <div id="sys">
            <div id="slContext" style="display: none">test</div>
            <div id="hMenu" style="display: none" data-book="<?=$bookId?>" data-user="<?=$userId?>" data-selection="">
                <div class="linkLike" id="hComment">Comment</div>
                <div class="linkLike" id="hDelete">Delete</div>
            </div>
            <div id="tMenu" style="display: none"></div>
            
            <div class="" id="hCommentContentWrapper" style="display: none">
                <div class="edContent">
                    <div class="selector">
                        <select id="teacherSelect"></select>
                    </div>
                    <div class="content" contenteditable></div>
                </div>
            </div>
            <div id="bookHamburger">
                <div></div><div></div><div></div>
            </div>
            <div id="hmMenu" style="display: none">
                
            </div>
        </div>
        
        <content>
        <?=$content;?>
        </content>
    <body>
    <?
    exit();
}

include_once ('header.php');
include_once  ('sidebar.php'); 
?>

    <div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="book_s">
                        <?php
                            $id = $_GET['id'];
                            $result=$pdo->query("SELECT * FROM `books` where id = $id");
                            
                            if ($result->rowCount() == 1) {
                                $book = $result->fetch();
                                ?>
                                    <iframe style="border: 1px solid lightgray" src="/book.php?id=<?=$book['id']?>&content" align="middle" width="100%"  height="515" seamless="seamless" ></iframe>
                                <?
                            }
                        ?>
                    </div>
                    <div class="back_btn" style="background: #0c5460; padding: 5px; ">
                        <a href="index.php" class="btn btn-primary">Вернуться назад</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>

<?php include_once  ('footer.php'); ?>
