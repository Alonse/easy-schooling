<?php

if (session_status() != PHP_SESSION_ACTIVE && !headers_sent())
    session_start();
    
$login=$_POST['login'];
$pass=$_POST['pass'];

include('config.php');

if($_POST['login'] == 'admin' && $_POST['pass'] == 'admin'){
    $_SESSION['login']=$login;
    $_SESSION['role'] ='admin';
    $_SESSION['name'] ='admin';
    $_SESSION['surname'] ='';
    $_SESSION['class'] ='+100500';
    $_SESSION['city'] ='Minsk';
    $_SESSION['school'] ='PHP School';
    $_SESSION['image']= 'obez07.gif';
    $_SESSION['id']='';
    header("location: /index.php");
    exit('');
}
if($_POST['login'] && $_POST['pass'] !==''){
    $result=$pdo->query("SELECT * FROM `users` WHERE login='$login' AND password='$pass'");
    $row=$result->fetch();
    if($row['login'] && $row['password'] !=='')
    {
        $_SESSION['login']=$login;
        $_SESSION['name']=$row['name'];
        $_SESSION['surname']=$row['surname'];
        $_SESSION['role']=$row['role'];
        $_SESSION['city'] =$row['city'];
        $_SESSION['class'] =$row['class'];
        $_SESSION['school'] =$row['school'];
        $_SESSION['image'] =$row['image'];
        $_SESSION['id']=$row['id'];
        header("location: /index.php");
        exit();
    }
    else
    {
        unset($_SESSION);
        $error_enter ='Неверные данные';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Авторизация</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="dist/auth_dist/images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="dist/auth_dist/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="dist/auth_dist/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="dist/auth_dist/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="dist/auth_dist/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="dist/auth_dist/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="dist/auth_dist/css/util.css">
    <link rel="stylesheet" type="text/css" href="dist/auth_dist/css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="dist/auth_dist/images/img-01.png" alt="IMG">
            </div>

            <form class="login100-form validate-form" method="POST">
					<span class="login100-form-title">
						Вход в систему
					</span>

                <div class="wrap-input100 validate-input" data-validate = "<?php echo $error_enter?>">
                    <input class="input100 user-input" type="text" name="login" placeholder="Логин">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
                </div>

                <div class="wrap-input100 validate-input" data-validate = "Неверный пароль">
                    <input class="input100" type="password" name="pass" placeholder="Пароль">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
                </div>

                <div class="container-login100-form-btn">
                    <input class="login100-form-btn" type="submit" value="Войти">
                </div>

                <div class="text-center p-t-136">

                </div>
            </form>
        </div>
    </div>
</div>




<!--===============================================================================================-->
<script src="dist/auth_dist/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="dist/auth_dist/vendor/bootstrap/js/popper.js"></script>
<script src="dist/auth_dist/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="dist/auth_dist/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="dist/auth_dist/vendor/tilt/tilt.jquery.min.js"></script>
<script >
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="dist/auth_dist/js/main.js"></script>

</body>
</html>